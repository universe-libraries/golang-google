package redis

import (
	"github.com/go-redis/redis"
)

// Get key
func (c Client) Get(key string) string {
	val, err := c.Redis.Get(key).Result()
	if err == redis.Nil {
		c.Log.Errorf("%s does not exist", key)
	} else if err != nil {
		c.Log.Errorln(err)
	}

	return val
}

// Delete Key
func (c Client) Delete(key string) {
	_, err := c.Redis.Del(key).Result()
	if err == redis.Nil {
		c.Log.Errorf("%s does not exist", key)
	} else if err != nil {
		c.Log.Errorln(err)
	}
}
