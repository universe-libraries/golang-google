module gitlab.com/universe-internal/libraries/golang-google/memorystore

go 1.12

require (
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/sirupsen/logrus v1.4.2
)
