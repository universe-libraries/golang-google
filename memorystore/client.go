package redis

import (
	"log"
	"os"
	"strconv"

	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
)

// Client Redis
type Client struct {
	Redis *redis.Client
	Log   *logrus.Entry
}

// NewClient Redis
func NewClient() (Client, error) {
	db, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		return Client{}, err
	}

	client := redis.NewClient(&redis.Options{
		Addr:       os.Getenv("REDIS_ADDR"),
		Password:   os.Getenv("REDIS_PASSWORD"),
		DB:         db,
		MaxRetries: 3,
	})

	_, err = client.Ping().Result()
	if err != nil {
		return Client{}, err
	}

	return Client{
		Redis: client,
		Log:   setLogLevel(logrus.WarnLevel),
	}, nil
}

func setLogLevel(level logrus.Level) *logrus.Entry {
	// Log as JSON instead of the default ASCII formatter.
	logrus.SetFormatter(&logrus.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logrus.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	logrus.SetLevel(level)

	return logrus.WithFields(logrus.Fields{
		"source": "redis",
	})
}

func init() {
	envs := []string{
		"REDIS_ADDR",
		"REDIS_PASSWORD",
		"REDIS_DB",
	}

	for _, env := range envs {
		if os.Getenv(env) == "" {
			log.Fatalf("Env variable %s is empty or unknown", env)
		}
	}
}
