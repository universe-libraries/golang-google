package bigquery

import (
	"time"

	"cloud.google.com/go/bigquery"
)

// Table structure
type Table struct {
	Name        string
	Description string

	// NEWLINE_DELIMITED_JSON
	Format     bigquery.DataFormat
	Source     []string
	Schema     bigquery.Schema
	Expiration time.Time
}

// Request data in BigQuery
func (c Client) Request(query string) (*bigquery.RowIterator, error) {
	q := c.BIGQUERY.Query(query)
	it, err := q.Read(c.CTX)
	if err != nil {
		return nil, err
	}

	return it, nil
}

// CreateExternalTable on Google Cloud
func (c Client) CreateExternalTable(dataset string, table Table) error {
	tableRef := c.BIGQUERY.Dataset(dataset).Table(table.Name)
	if err := tableRef.Create(
		c.CTX,
		&bigquery.TableMetadata{
			Name:        table.Name,
			Description: table.Description,
			ExternalDataConfig: &bigquery.ExternalDataConfig{
				SourceFormat:        table.Format,
				SourceURIs:          table.Source,
				Schema:              table.Schema,
				AutoDetect:          false,
				Compression:         "NONE",
				IgnoreUnknownValues: true,
			},
			ExpirationTime: table.Expiration,
		},
	); err != nil {
		return err
	}

	return nil
}

// CreateTablePartitioned on Google Cloud
func (c Client) CreateTablePartitioned(dataset string, table Table) error {
	tableRef := c.BIGQUERY.Dataset(dataset).Table(table.Name)
	if err := tableRef.Create(
		c.CTX,
		&bigquery.TableMetadata{
			TimePartitioning: &bigquery.TimePartitioning{
				Field:                  "time",
				RequirePartitionFilter: false,
			},
			Schema: table.Schema,
		},
	); err != nil {
		return err
	}

	return nil
}

// CreateDataset on Google Cloud
func (c Client) CreateDataset(name string, metadata bigquery.DatasetMetadata) error {
	return c.BIGQUERY.Dataset(name).Create(c.CTX, &metadata)
}
