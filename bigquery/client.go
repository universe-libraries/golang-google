package bigquery

import (
	"context"
	"log"
	"os"

	"cloud.google.com/go/bigquery"
)

// Client struct
type Client struct {
	BIGQUERY *bigquery.Client
	CTX      context.Context
}

// NewClient on BigQuery
func NewClient() (Client, error) {
	ctx := context.Background()
	client, err := bigquery.NewClient(ctx, os.Getenv("GCP_PROJECT"))
	if err != nil {
		return Client{}, err
	}

	return Client{
		BIGQUERY: client,
		CTX:      ctx,
	}, nil
}

func init() {
	// GOOGLE_APPLICATION_CREDENTIALS needed
	envs := []string{
		"GCP_PROJECT",
	}

	for _, env := range envs {
		if os.Getenv(env) == "" {
			log.Fatalf("Env variable %s is empty or unknown", env)
		}
	}
}
