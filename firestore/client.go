package firestore

import (
	"context"
	"log"
	"os"

	"cloud.google.com/go/firestore"
)

// FirestoreClient struct
type FirestoreClient struct {
	CTX    context.Context
	Client *firestore.Client
}

// NewConnect on Firestore
func NewConnect() FirestoreClient {
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, os.Getenv("GOOGLE_PROJECT"))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	return FirestoreClient{
		CTX:    ctx,
		Client: client,
	}
}
