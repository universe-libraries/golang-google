package pubsub

import (
	"context"
	"log"
	"os"

	"cloud.google.com/go/pubsub"
)

// Receive message on topic
func (c Client) Receive(name string) ([]byte, error) {
	sub := c.PubSub.SubscriptionInProject(name, os.Getenv("GCP_PROJECT"))
	if err := sub.Receive(c.CTX, func(ctx context.Context, m *pubsub.Message) {
		log.Printf("Got message: %s", m.Data)
		m.Ack()
	}); err != nil {
		return nil, err
	}

	return nil, nil
}

// Push message on topic
func (c Client) Push(name string, data []byte, attributes map[string]string) error {
	topic := c.PubSub.TopicInProject(name, os.Getenv("GCP_PROJECT"))
	topic.Publish(c.CTX, &pubsub.Message{Attributes: attributes, Data: data})
	topic.Stop()

	return nil
}
