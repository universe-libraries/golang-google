package pubsub

import (
	"context"
	"log"
	"os"

	"cloud.google.com/go/pubsub"
)

// Client def struct
type Client struct {
	CTX               context.Context
	PubSub            *pubsub.Client
	Topic, Subscriber string
}

// NewClient Google Cloud PubSub
func NewClient() (Client, error) {
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, os.Getenv("GCP_PROJECT"))
	if err != nil {
		return Client{}, err
	}

	return Client{
		CTX:    ctx,
		PubSub: client,
	}, nil
}

func init() {
	// GOOGLE_APPLICATION_CREDENTIALS needed
	envs := []string{
		"GCP_PROJECT",
	}

	for _, env := range envs {
		if os.Getenv(env) == "" {
			log.Fatalf("Env variable %s is empty or unknown", env)
		}
	}
}
