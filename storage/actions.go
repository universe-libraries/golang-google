package storage

import (
	"io"
	"io/ioutil"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

// Object structure
type Object struct {
	Name        string
	ContentType string
	MD5         []byte
	Class       string
	Size        int64
}

// ListObjects in bucket
func (c Client) ListObjects(bucket, path string) ([]Object, error) {
	var objects []Object

	it := c.GCS.Bucket(bucket).Objects(c.CTX, &storage.Query{
		Prefix: path,
	})

	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}

		if err != nil {
			return []Object{}, err
		}

		objects = append(objects, Object{
			Name:        attrs.Name,
			MD5:         attrs.MD5,
			Class:       attrs.StorageClass,
			Size:        attrs.Size,
			ContentType: attrs.ContentType,
		})
	}

	return objects, nil
}

// Read object in buckets
func (c Client) Read(bucket, object string) ([]byte, error) {
	rc, err := c.GCS.Bucket(bucket).Object(object).NewReader(c.CTX)
	if err != nil {
		return nil, err
	}
	defer rc.Close()

	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// Write content in object GCS
func (c Client) Write(bucket, object string, content io.ReadCloser) error {
	wc := c.GCS.Bucket(bucket).Object(object).NewWriter(c.CTX)
	if _, err := io.Copy(wc, content); err != nil {
		return err
	}

	if err := wc.Close(); err != nil {
		return err
	}

	return nil
}

// Delete object in GCS
func (c Client) Delete(bucket, object string) error {
	return c.GCS.Bucket(bucket).Object(object).Delete(c.CTX)
}
