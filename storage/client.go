package storage

import (
	"context"

	"cloud.google.com/go/storage"
)

// Client structure
type Client struct {
	CTX context.Context
	GCS *storage.Client
}

// NewClient storage
func NewClient() (Client, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return Client{}, err
	}

	return Client{
		CTX: ctx,
		GCS: client,
	}, nil
}
